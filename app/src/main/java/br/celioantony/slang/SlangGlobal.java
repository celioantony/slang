package br.celioantony.slang;

import models.Languages;
import models.Users;

/**
 * Created by Celio Antony on 29/11/2016.
 */
public class SlangGlobal {
    private Users user;
    private String keyUsr;
    private Languages languages;

    private static SlangGlobal ourInstance = new SlangGlobal();

    public static SlangGlobal getInstance() {
        return ourInstance;
    }

    private SlangGlobal() {
    }

    public Users getUser() {
        return user;
    }

    public void setUser(Users user) {
        this.user = user;
    }

    public String getKeyUsr() {
        return keyUsr;
    }

    public void setKeyUsr(String keyUsr) {
        this.keyUsr = keyUsr;
    }

    public Languages getLanguages() {
        return languages;
    }

    public void setLanguages(Languages languages) {
        this.languages = languages;
    }
}
