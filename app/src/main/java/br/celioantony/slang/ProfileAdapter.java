package br.celioantony.slang;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import models.Comment;
import models.Post;
import models.Users;

import java.net.URI;
import java.util.ArrayList;

/**
 * Created by Celio Antony on 23/11/2016.
 */

public class ProfileAdapter extends RecyclerView.Adapter<ProfileAdapter.ViewHolder> {

    FirebaseDatabase database = FirebaseDatabase.getInstance();
    FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
    final DatabaseReference refComments = database.getReference("Comments");
    ArrayList<Comment> comments = new ArrayList<>();
    SlangGlobal slang = SlangGlobal.getInstance();
    Context ctx;
    private ArrayList<Post> posts;
    private String user;
    private String imgPost;
    OnClickComment onClickComment;

    public void setPost(ArrayList<Post> post) {
        this.posts = post;
        this.notifyDataSetChanged();
    }

    public interface OnClickComment {
        void onClickLitener(int position, ArrayList<Post> posts);
    }

    public void setComment(OnClickComment comment) {
        this.onClickComment = comment;
    }

    public ProfileAdapter(Context ctx, ArrayList<Post> posts) {
        this.ctx = ctx;
        this.posts = posts;
    }

    @Override
    public ProfileAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.posts, parent, false);
        final ViewHolder viewHolder = new ViewHolder(view);

        viewHolder.comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickComment.onClickLitener(viewHolder.getAdapterPosition(), posts);
            }
        });

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ProfileAdapter.ViewHolder holder, int position) {
        holder.setTxtUsername(user);
        holder.setTxtQuestion(posts.get(position).getCode(), position);
        holder.setTxtWords(posts.get(position).getWords() + " ?");
        holder.setImgPost(imgPost);
        holder.setNumberComments(position);

    }

    @Override
    public int getItemCount() {
        return posts.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView txtUsername;
        TextView txtQuestion;
        TextView txtWords;
        TextView txtNumberComments;
        ImageView imgPost;
        CardView comment;


        public ViewHolder(View itemView) {
            super(itemView);

            txtUsername = (TextView) itemView.findViewById(R.id.card_username);
            txtQuestion = (TextView) itemView.findViewById(R.id.card_question);
            txtWords = (TextView) itemView.findViewById(R.id.card_words);
            imgPost = (ImageView) itemView.findViewById(R.id.card_photo_profile);
            comment = (CardView) itemView.findViewById(R.id.card_main);
            txtNumberComments = (TextView) itemView.findViewById(R.id.card_comments);


        }

        public void setTxtUsername(String username) {
            if (txtUsername == null) return;
            txtUsername.setText(slang.getUser().getUsername());
        }

        public void setTxtWords(String words){
            if (txtWords == null) return;
            txtWords.setText(words);
        }

        public void setTxtQuestion(String question, int position) {
            if(question == null) return;

            if(question.equals("01")) txtQuestion.setText(ctx.getResources().getString(R.string.htst)+" "+posts.get(position).getLanguage());

            if(question.equals("02")) txtQuestion.setText(ctx.getResources().getString(R.string.wdyt)+" "+posts.get(position).getLanguage());

            if(question.equals("03")) txtQuestion.setText(ctx.getResources().getString(R.string.witm)+" "+posts.get(position).getLanguage());
        }

        public void setImgPost(String url) {
            Picasso.with(ctx).load(firebaseUser.getPhotoUrl()).into(imgPost);
        }

        public void setNumberComments(int position) {

            final ArrayList<Comment> comments = new ArrayList<>();

            refComments.child(slang.getUser().getUuid()).child(posts.get(position).getId())
                    .addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            comments.clear();
                            for(DataSnapshot data: dataSnapshot.getChildren()){
                                comments.add(data.getValue(Comment.class));
                            }
                            txtNumberComments.setText(comments.size()+"  Comentários");
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            Log.i("SLANG", "ProfileAdapter onCancelled"+ databaseError);
                        }
                    });
        }
    }
}
