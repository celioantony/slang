package br.celioantony.slang;

import android.app.DialogFragment;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;

import models.Comment;
import models.Post;

/**
 * Created by Celio Antony on 29/11/2016.
 */

public class DialogComments extends DialogFragment {

    FirebaseDatabase database = FirebaseDatabase.getInstance();
    FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
    SlangGlobal slang = SlangGlobal.getInstance();
    final DatabaseReference refComments = database.getReference("Comments");

    RecyclerView recyclerView;
    ArrayList<Comment> comments = new ArrayList<>();
    CommentsAdapter commentsAdapter;
    Comment comment;
    Post currentPost;

    public DialogComments (Post currentPost) {
        this.currentPost = currentPost;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_dialog_comments, container, false);
        getDialog().setTitle("Simple Dialog");
        comment = new Comment();

        commentsAdapter = new CommentsAdapter(comments);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.dialog_recycler_comments);
        recyclerView.setAdapter(commentsAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(rootView.getContext(), LinearLayoutManager.VERTICAL, true));

        final EditText editComment = (EditText)  rootView.findViewById(R.id.dialog_comment);
        ImageView btnSend = (ImageView) rootView.findViewById(R.id.dialog_send_comment);
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(editComment.getText().toString() != ""){
                    comment.setUsername(slang.getUser().getUsername());
                    comment.setPhoto(firebaseUser.getPhotoUrl().toString());
                    comment.setDetails(editComment.getText().toString());
                    comment.setDatetime(DateFormat.getDateTimeInstance().format(new Date()));
                    editComment.setText("");

                    refComments.child(slang.getUser().getUuid()).child(currentPost.getId())
                    .push().setValue(comment);
                }
            }
        });

        CommentListener();

        return rootView;
    }

    public void CommentListener(){
        refComments.child(slang.getUser().getUuid()).child(currentPost.getId())
        .addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                comments.clear();
                Log.i("SLANG", "onDataChange: "+dataSnapshot.getValue());
                for(DataSnapshot data: dataSnapshot.getChildren()){
                    Log.i("SLANG", "DATA Profile: "+data);
                    comments.add(data.getValue(Comment.class));
                }

                commentsAdapter.setComment(comments);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.i("SLANG", "ERROR: "+databaseError);
            }
        });
    }

}
