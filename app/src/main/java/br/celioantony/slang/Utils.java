package br.celioantony.slang;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.UUID;

import models.Comment;
import models.Post;
import models.Users;

/**
 * Created by Celio Antony on 26/11/2016.
 */

public class Utils {

//    // reference database
//    FirebaseDatabase database = FirebaseDatabase.getInstance();
//    SlangGlobal slang = SlangGlobal.getInstance();
//    final DatabaseReference refUsers = database.getReference("Users");
//    final DatabaseReference refPosts = database.getReference("Publish");
//    final DatabaseReference refComments = database.getReference("Comments");
//
//    // local var
//    Activity activity;
//    Fragment frag;
//    private Users usr;
//
//    public Utils(){
//    }

//    public Utils(Activity activity){
//        this.activity = activity;
////        this.dataUser = dataUser;
//    }

//    public Utils(Fragment frag){
//        this.frag = frag;
//    }
//
//
//    private Query compareEmail(String email) {
//        Query q = refUsers.orderByChild("email").equalTo(email);
//
//        return q;
//    }

//    private void pushUser(Users user) {
//        refUsers.push().setValue(user);
//    }

//    public void createUserNotExists(final String username, final String emailUser) {
//        Query q = compareEmail(emailUser);
//
//        q.addListenerForSingleValueEvent(new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
//                Log.i("onDataChange", ""+dataSnapshot.getValue());
//                Users user = new Users(username, UUID.randomUUID().toString(), emailUser, "", "", 0, 0);
//                if(dataSnapshot.getValue() == null) {
//                    pushUser(user);
//                }
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//                Log.i("onCancelled", ""+databaseError);
//            }
//        });
//    }

//    public void getDataUser(String emailUser) {
//        Query q = refUsers.orderByChild("email").equalTo(emailUser);
//
//        q.addListenerForSingleValueEvent(new ValueEventListener() {
//
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
//                Log.i("getDataUser", ""+dataSnapshot);
//                Log.i("getDataUser getKey", ""+dataSnapshot.getKey());
//                Log.i("getDataUser getValue", ""+dataSnapshot.getValue());
//                Log.i("getDataUser getChildren", ""+dataSnapshot.getChildren());
//                for(DataSnapshot data: dataSnapshot.getChildren()) {
//                    SlangGlobal.getInstance().setUser(data.getValue(Users.class));
//                }
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//            }
//        });
//    }
//
//    public void getPosts() {
//        Query q = refPosts.child(slang.getUser().getUuid());
//
//        q.addListenerForSingleValueEvent(new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
//                ArrayList<Post> posts = new ArrayList<>();
//                for(DataSnapshot data: dataSnapshot.getChildren()){
//                    Log.i("DATA: ", ""+data.getValue());
//                    posts.add(data.getValue(Post.class));
//                }
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//                Log.i("onCancelled", "");
//            }
//        });
//    }
//
//    public void getCommentsPostUser(String uuidUser) {
//        Log.i("getCommentsPostUser", "");
//        Query q = refComments.child(uuidUser);
//
//        q.addListenerForSingleValueEvent(new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
//                Log.i("onDataChange", ""+dataSnapshot);
//                Log.i("getChildren", ""+dataSnapshot.getChildren());
//                ArrayList<Comment> comments = new ArrayList<>();
//                for(DataSnapshot data: dataSnapshot.getChildren()){
//                    Log.i("DATA: ", ""+data.getValue());
//                    comments.add(data.getValue(Comment.class));
//                }
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//                Log.i("onCancelled", "");
////                callback.errorPosts();
//            }
//        });
//    }

    // method for build layout
//    public void buildHeadProfile(Users user) {
//        Log.i("buildHeadProfile","");
//        TextView txtCountry = (TextView) this.frag.getView().findViewById(R.id.profile_country),
//                 txtAbout = (TextView) this.frag.getView().findViewById(R.id.profile_about),
//                 txtFollowers = (TextView) this.frag.getView().findViewById(R.id.frag_followers_count),
//                 txtFollowing = (TextView) this.frag.getView().findViewById(R.id.frag_following_count);
//
//        txtCountry.setText(user.getCountry());
//        txtAbout.setText(user.getAbout());
//        txtFollowers.setText(""+user.getFollowers());
//        txtFollowing.setText(""+user.getFollowing());
//    }
}
