package br.celioantony.slang;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.RadioButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import models.Languages;
import models.Post;
import models.Users;

public class Questions extends AppCompatActivity {

    FirebaseDatabase database = FirebaseDatabase.getInstance();
    SlangGlobal slang = SlangGlobal.getInstance();
    final DatabaseReference refLanguages = database.getReference("Languages").child(slang.getUser().getUuid());
    final DatabaseReference refUser = database.getReference("Users");
    final DatabaseReference refPublish = database.getReference("Publish");

    Date date = new Date();
    ProgressDialog progressDialog;
    private String email;
    private String questionDescription;
    private String code;
    private String words;
    private Users user;
    private String languageSelected = null;

    private TextView txtQuestion;
    private TextView txtWords;
    private TextView txtLanguage;
    RadioButton language_1;
    RadioButton language_2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_questions);


        txtQuestion = (TextView) findViewById(R.id.question_description);
        txtWords = (TextView) findViewById(R.id.question_words);
//        txtLanguage = (TextView) findViewById(R.id.question_language);
        language_1 = (RadioButton) findViewById(R.id.radio_lang_1);
        language_2 = (RadioButton) findViewById(R.id.radio_lang_2);

        languages();



        Bundle bundle = getIntent().getExtras();
        questionDescription = ""+bundle.getString("question");
//        getReferenceUser(bundle.getString("email"));


        txtQuestion.setText(questionDescription);
//        txtLanguage.setText("Inglês");
        email = bundle.getString("email");
        code = bundle.getString("code");
    }

    public void validPublish(View view) {
        words = txtWords.getText().toString();
        if (words.matches("") || languageSelected == null) {
            Toast.makeText(this, "Conteúdo vázio!", Toast.LENGTH_SHORT).show();
            return;
        } else {
            progressDialog = new ProgressDialog(this);
            progressDialog.setMessage("Publicando...");
            progressDialog.setCancelable(false);
            progressDialog.show();

            Post post = new Post();
            post.setId(UUID.randomUUID().toString());
            post.setCode(code);
            post.setWords(txtWords.getText().toString());
            post.setLanguage(languageSelected);
            post.setDatetime(DateFormat.getDateTimeInstance().format(new Date()));
            upPublish(post);
        }
    }

    public void upPublish(Post post){
        DatabaseReference ref = refPublish.child(slang.getUser().getUuid()).child(post.getId());

        ref.setValue(post).addOnCompleteListener(this, new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                progressDialog.dismiss();
                txtWords.setText("");
                goToTimelineScreen();
            }
        });
    }

    public void goToTimelineScreen() {
        Intent intent = new Intent(this, Initial.class);
        startActivity(intent);
    }

    public void getReferenceUser(String emailUser) {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Processando...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        Query q = refUser.orderByChild("email").equalTo(emailUser);

        q.addListenerForSingleValueEvent(new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.i("ReferenceUser Value", ""+dataSnapshot.getValue());
                Log.i("ReferenceUser Children", ""+dataSnapshot.getChildren());
                for(DataSnapshot data: dataSnapshot.getChildren()) {

                    Log.i("msgSnapshop.getValue()", ""+data.getValue());
                    user = data.getValue(Users.class);
                    progressDialog.dismiss();
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.i("onCancelled", ""+databaseError);
            }
        });

    }

    public void onRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.radio_lang_1:
                if (checked)
                    languageSelected = getResources().getString(R.string.english);
                    break;
            case R.id.radio_lang_2:
                if (checked)
                    languageSelected = getResources().getString(R.string.spanish);
                    break;
        }
    }

//    public void CMM(View view) {
//        Log.i("CLICK EVENT", "-");
//        final DatabaseReference ref = database.getReference("Publish").child(user.getUuid())
//                .child("2c480dd9-fd5e-4086-933b-d3271828c37d").child("comments");
//
//        Map<String, Object> comment = new HashMap<String, Object>();
//        String idComment = UUID.randomUUID().toString();
//        comment.put("id", idComment);
//        comment.put("details", "my comment");
//        ref.child(idComment).updateChildren(comment);
//
//    }

    public void languages() {

        if(slang.getLanguages() == null){
            goToTimelineScreen();
            Toast.makeText(getApplication().getBaseContext(), "Configure o idioma!",
                    Toast.LENGTH_SHORT).show();
        }else {
            for (int i = 0; i <  slang.getLanguages().getLanguages().size(); i++) {
                if(slang.getLanguages().getLanguages().get(i)
                        .equals(getResources().getString(R.string.english))){
                    Log.i("SLANG", "IDIOMA"+slang.getLanguages().getLanguages().get(i));
                    language_1.setVisibility(View.VISIBLE);
                }
                if(slang.getLanguages().getLanguages().get(i)
                        .equals(getResources().getString(R.string.spanish))){
                    Log.i("SLANG", "IDIOMA"+slang.getLanguages().getLanguages().get(i));
                    language_2.setVisibility(View.VISIBLE);
                }
            }
        }


    }

}
