package br.celioantony.slang;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;

import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.TextView;
import android.widget.Toast;

import com.facebook.login.LoginManager;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import models.Languages;
import models.Users;

public class Initial extends AppCompatActivity {

    ProgressDialog progressDialog;
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
    SlangGlobal slang = SlangGlobal.getInstance();

    private TextView nameUserTv;
    private TextView emailUserTv;
    private TextView uidUserTv;

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    // instance tablayout
    private TabLayout tabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_initial);

        if(firebaseUser != null) {
            Log.i("GET DATA USER", "INITIAL");
            getDataUser(firebaseUser.getEmail());
//            logoutFacebookAndFirebase();
        } else {
            goLoginScreen();
        }

        // get element tablayout in tamplate
        tabLayout = (TabLayout) findViewById(R.id.tablayout);
        // create instance tabs
        tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.enabled_timeline));
        tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.disabled_post));
        tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.disabled_profile));
        tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.disabled_settings));



//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager(), tabLayout.getTabCount());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        mViewPager.addOnPageChangeListener(
                new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                mViewPager.setCurrentItem(tab.getPosition());
                Log.i("GET POSITION", ""+tab.getPosition());
                if(tab.getPosition() == 0){
                    Log.i("HERE", "0");
                    tab.setIcon(R.drawable.enabled_timeline);
                }
                if(tab.getPosition() == 1){
                    Log.i("HERE", "1");
                    tab.setIcon(R.drawable.enabled_post);
                }
                if(tab.getPosition() == 2){
                    Log.i("HERE", "2");
                    tab.setIcon(R.drawable.enabled_profile);
                }
                if(tab.getPosition() == 3){
                    Log.i("HERE", "3");
                    tab.setIcon(R.drawable.enabled_settings);
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                Log.i("UNSELECTED POSITION", ""+tab.getPosition());
                if(tab.getPosition() == 0){
                    Log.i("HERE", "0");
                    tab.setIcon(R.drawable.disabled_timeline);
                }
                if(tab.getPosition() == 1){
                    Log.i("HERE", "1");
                    tab.setIcon(R.drawable.disabled_post);
                }
                if(tab.getPosition() == 2){
                    Log.i("HERE", "2");
                    tab.setIcon(R.drawable.disabled_profile);
                }
                if(tab.getPosition() == 3){
                    Log.i("HERE", "3");
                    tab.setIcon(R.drawable.disabled_settings);
                }
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                Log.i("info", "onTabReselected");
            }
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_tabs, menu);
        MenuItem searchViewItem = menu.findItem(R.id.action_search);
        final SearchView searchViewAndroidActionBar =
                (SearchView) MenuItemCompat.getActionView(searchViewItem);
        searchViewAndroidActionBar.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                searchViewAndroidActionBar.clearFocus();
                Toast.makeText(getBaseContext(), "SHOW", Toast.LENGTH_SHORT).show();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch (id) {
            case R.id.action_logout:
                logoutFacebookAndFirebase();
                return true;

            case R.id.action_search:
                break;
        }

//        if (id == R.id.action_logout) {
//            logoutFacebookAndFirebase();
//            return true;
//        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_initial, container, false);
            TextView textView = (TextView) rootView.findViewById(R.id.section_label);
            textView.setText(getString(R.string.section_format, getArguments().getInt(ARG_SECTION_NUMBER)));
            return rootView;
        }
    }

    private void goLoginScreen() {
        Intent intent = new Intent(this, Login.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    public void goToQuestion1Screen(View view) {
        Intent intent = new Intent(this, Questions.class);
        intent.putExtra("code", "01");
        intent.putExtra("email", firebaseUser.getEmail());
        intent.putExtra("question", getResources().getString(R.string.htst));
        startActivity(intent);
    }

    public void goToQuestion2Screen(View view) {
        Intent intent = new Intent(this, Questions.class);
        intent.putExtra("code", "02");
        intent.putExtra("email", firebaseUser.getEmail());
        intent.putExtra("question", getResources().getString(R.string.wdyt));
        startActivity(intent);
    }

    public void goToQuestion3Screen(View view) {
        Intent intent = new Intent(this, Questions.class);
        intent.putExtra("code", "03");
        intent.putExtra("email", firebaseUser.getEmail());
        intent.putExtra("question", getResources().getString(R.string.witm));
        startActivity(intent);
    }

    public void getDataUser(String emailUser) {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Carregando");
        progressDialog.setCancelable(false);
        progressDialog.show();


        final DatabaseReference refUsers = database.getReference("Users");
        final DatabaseReference refLanguages = database.getReference("Languages");
        Query q = refUsers.orderByChild("email").equalTo(emailUser);

        q.addListenerForSingleValueEvent(new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.i("SLANG","GET DATA USER IN INITIAL");
                for(DataSnapshot data: dataSnapshot.getChildren()) {
                    SlangGlobal.getInstance().setUser(data.getValue(Users.class));
                    SlangGlobal.getInstance().setKeyUsr(data.getKey());
                    Log.i("SLANG", "KEY USER: "+ data.getKey());
                }
                refLanguages.child(slang.getUser().getUuid())
                    .addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            if(dataSnapshot.getValue() != null){
                                Languages lang = dataSnapshot.getValue(Languages.class);
                                slang.setLanguages(lang);
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });

                Log.i("SLANG", ""+SlangGlobal.getInstance().getKeyUsr());
                Log.i("SLANG", ""+SlangGlobal.getInstance().getUser().getUsername());
                progressDialog.dismiss();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Toast.makeText(Initial.this, "Error ao carregar a aplicação.", Toast.LENGTH_SHORT).show();
                logoutFacebookAndFirebase();
            }
        });
    }

    public void logoutFacebookAndFirebase() {
        FirebaseAuth.getInstance().signOut();
        LoginManager.getInstance().logOut();
        goLoginScreen();
    }
}
