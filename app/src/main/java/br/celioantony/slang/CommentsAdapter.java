package br.celioantony.slang;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import models.Comment;
import models.Post;

/**
 * Created by Celio Antony on 29/11/2016.
 */

public class CommentsAdapter extends RecyclerView.Adapter<CommentsAdapter.ViewHolder> {

    FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
    SlangGlobal slang = SlangGlobal.getInstance();
    Context ctx;
    private String username;
    private String imgProfile;
    private ArrayList<Comment> comments;

    public CommentsAdapter(ArrayList<Comment> comments){
//        this.ctx = ctx;
//        this.username = slang.getUser().getUsername();
//        this.imgProfile = imgProfile;
        this.comments = comments;
    }

    public void setComment(ArrayList<Comment> comments) {
        this.comments = comments;
        this.notifyDataSetChanged();
    }

    @Override
    public CommentsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.comment, parent, false);
        final ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(CommentsAdapter.ViewHolder holder, int position) {
        holder.setImgProfileComment(comments.get(position).getPhoto());
        holder.setTxtUsername(comments.get(position).getUsername());
        holder.setComment(comments.get(position).getDetails());
    }

    @Override
    public int getItemCount() {
        return comments.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView imgProfileComment;
        TextView txtUsername;
        TextView txtComment;

        public ViewHolder(View itemView) {
            super(itemView);

            imgProfileComment = (ImageView) itemView.findViewById(R.id.comment_profile_image);
            txtUsername = (TextView) itemView.findViewById(R.id.comment_username);
            txtComment = (TextView) itemView.findViewById(R.id.comment_details);
        }

        public void setImgProfileComment(String url){
            Picasso.with(ctx).load(firebaseUser.getPhotoUrl()).into(imgProfileComment);
        }

        public void setTxtUsername(String username) {
            txtUsername.setText(username);
        }

        public void setComment(String comment) {
            txtComment.setText(comment);
        }
    }
}
