package br.celioantony.slang;

import models.Comment;
import models.Post;
import models.Users;

import java.util.ArrayList;

/**
 * Created by Celio Antony on 28/11/2016.
 */

public interface UserListener {

    void successUser();
    void errorUser();

}
