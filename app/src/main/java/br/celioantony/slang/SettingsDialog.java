package br.celioantony.slang;

import android.app.DialogFragment;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.text.InputFilter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import models.Languages;

/**
 * Created by Celio Antony on 30/11/2016.
 */

public class SettingsDialog extends DialogFragment {

    FirebaseDatabase database = FirebaseDatabase.getInstance();
    SlangGlobal slang = SlangGlobal.getInstance();
    DatabaseReference refUser = database.getReference("Users/"+slang.getKeyUsr());
    DatabaseReference refLanguages = database.getReference("Languages").child(slang.getUser().getUuid());


    String about;
    String title;
    int component;
    TextView txtTitle;
    EditText editDialog;
    Switch switchLanguage_1;
    Switch switchLanguage_2;
    Button btnSave;
    Button btnCancel;

    public SettingsDialog(String title, int component) {
        this.title = title;
        this.component = component;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_dialog_settings, container, false);
        getDialog().setTitle("Simple Dialog");

        txtTitle = (TextView) rootView.findViewById(R.id.dialog_title_action);
        editDialog = (EditText) rootView.findViewById(R.id.dialog_edit);
        switchLanguage_1 = (Switch) rootView.findViewById(R.id.dialog_switch_language_1);
        switchLanguage_2 = (Switch) rootView.findViewById(R.id.dialog_switch_language_2);
//        editUsername = (EditText) rootView.findViewById(R.id.dialog_edit_username);
//        editAbout = (EditText) rootView.findViewById(R.id.dialog_edit_about);
//        editNationality = (EditText) rootView.findViewById(R.id.dialog_edit_nationality);
        btnSave = (Button) rootView.findViewById(R.id.dialog_save);
        btnCancel = (Button) rootView.findViewById(R.id.dialog_cancel);

        if(component == R.id.settings_card_about){
            txtTitle.setText("Sobre você"); editDialog.setVisibility(View.VISIBLE); switchLanguage_1.setVisibility(View.GONE); switchLanguage_2.setVisibility(View.GONE);
        }
        if(component == R.id.settings_card_username){
            txtTitle.setText("Nome de usuário");
            editDialog.setVisibility(View.VISIBLE); switchLanguage_1.setVisibility(View.GONE); switchLanguage_2.setVisibility(View.GONE);
        }
        if(component == R.id.settings_card_nationality){
            txtTitle.setText("Nacionalidade"); editDialog.setVisibility(View.VISIBLE); switchLanguage_1.setVisibility(View.GONE); switchLanguage_2.setVisibility(View.GONE);
        }
        if(component == R.id.settings_card_language){
            txtTitle.setText("Idioma para aprender"); editDialog.setVisibility(View.GONE); switchLanguage_1.setVisibility(View.VISIBLE); switchLanguage_2.setVisibility(View.VISIBLE);

            refLanguages.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    ArrayList<String> currentLang = new ArrayList<>();
                    Log.i("SLANG", "LanguagesListener: "+dataSnapshot.getValue());


                    if(dataSnapshot.getValue() != null){
                        Languages lang = dataSnapshot.getValue(Languages.class);
                        currentLang = lang.getLanguages();
                        for (int i = 0; i < currentLang.size(); i++){
                            if(currentLang.get(i).equals("Inglês")){
                                Log.i("SLANG", "INGLÊS");
                                switchLanguage_1.setChecked(true);
                            }
                            if(currentLang.get(i).equals("Espanhol")){
                                Log.i("SLANG", "ESPANHOL");
                                switchLanguage_2.setChecked(true);
                            }
                        }
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    Log.i("SLANG", "onCancelled: "+databaseError);
                }
            });
        }



        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(component == R.id.settings_card_about) setAbout(rootView);
                if(component == R.id.settings_card_username) setUsername(rootView);
                if(component == R.id.settings_card_nationality) setNationality(rootView);
                if(component == R.id.settings_card_language) setLanguage(rootView);
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switchLanguage_1.setVisibility(View.GONE);
                switchLanguage_2.setVisibility(View.GONE);
                dismiss();
            }
        });

        return rootView;
    }

    private void setAbout(final View view) {
        String about = editDialog.getText().toString();
        Map<String, Object> userUpdates = new HashMap<String, Object>();
        userUpdates.put("about", about);

        if(about == ""){
            Toast.makeText(view.getContext(), "Texto vázio!", Toast.LENGTH_SHORT).show();
        }else {
            refUser.updateChildren(userUpdates).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    Toast.makeText(view.getContext(), "Dados atualizados!", Toast.LENGTH_SHORT).show();
                    txtTitle.setVisibility(View.GONE);
                    dismiss();
                }
            });
        }
    }

    private void setUsername(final View view) {
        String username = editDialog.getText().toString();
        Map<String, Object> userUpdates = new HashMap<String, Object>();
        userUpdates.put("username", username);

        if(username == ""){
            Toast.makeText(view.getContext(), "Texto vázio!", Toast.LENGTH_SHORT).show();
        }else {
            refUser.updateChildren(userUpdates).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    Toast.makeText(view.getContext(), "Dados atualizados!", Toast.LENGTH_SHORT).show();
                    dismiss();
                }
            });
        }
    }

    private void setNationality(final View view) {
        String nationality = editDialog.getText().toString();
        Map<String, Object> userUpdates = new HashMap<String, Object>();
        userUpdates.put("country", nationality);

        if(nationality == ""){
            Toast.makeText(view.getContext(), "Texto vázio!", Toast.LENGTH_SHORT).show();
        }else {
            refUser.updateChildren(userUpdates).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    Toast.makeText(view.getContext(), "Dados atualizados!", Toast.LENGTH_SHORT).show();
                    dismiss();
                }
            });
        }
    }

    private void setLanguage(final View view) {
        ArrayList<String> slangs = new ArrayList<>();
        String language1 = switchLanguage_1.getText().toString();
        String language2 = switchLanguage_2.getText().toString();

        if(switchLanguage_1.isChecked()) {
            slangs.add(language1);
        }

        if(switchLanguage_2.isChecked()){
            slangs.add(language2);
        }

        if(slangs.size() != 0){
            Languages languages = new Languages(slangs);
            refLanguages.setValue(languages);
            Toast.makeText(view.getContext(), "Dados atualizados!", Toast.LENGTH_SHORT).show();
        }else {
            Toast.makeText(view.getContext(), "Não a alterações!", Toast.LENGTH_SHORT).show();
        }
        dismiss();
    }

}
