package br.celioantony.slang;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

import models.Post;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Profile.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Profile#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Profile extends Fragment implements ProfileAdapter.OnClickComment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    FirebaseDatabase database = FirebaseDatabase.getInstance();
    final DatabaseReference refPosts = database.getReference("Publish");
    FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
    SlangGlobal slang = SlangGlobal.getInstance();

    // Class
    ProfileAdapter profileAdapter;
    ArrayList<Post> posts = new ArrayList<>();



    // Layout
    TextView txtUsername;
    ImageView imgProfile;
    TextView txtNationality;
    TextView txtAbout;
    TextView txtFollowingCount;
    TextView txtFollowersCount;

//    TextView txtCountry, txtAbout, txtFollowers, txtFollowing;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public Profile() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Profile.
     */
    // TODO: Rename and change types and number of parameters
    public static Profile newInstance(String param1, String param2) {
        Profile fragment = new Profile();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        buildHeadProfile();

        profileAdapter = new ProfileAdapter(getContext(), posts);
        profileAdapter.setComment(this);
        RecyclerView recyclerView = (RecyclerView) getView().findViewById(R.id.recycler_profile);
        recyclerView.setAdapter(profileAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, true));

        refPosts.child(slang.getUser().getUuid()).orderByChild("datetime")
        .addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                posts.clear();
                Log.i("SLANG", "onDataChange Profile: "+dataSnapshot);
                for(DataSnapshot data: dataSnapshot.getChildren()){
                    Log.i("SLANG", "DATA Profile: "+data);
                    posts.add(data.getValue(Post.class));
                }

                profileAdapter.setPost(posts);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.i("SLANG", "onCancelled Profile");
            }
        });

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_profile, container, false);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void buildHeadProfile() {

        imgProfile = (ImageView) getView().findViewById(R.id.profile_image);
        txtUsername = (TextView) getView().findViewById(R.id.profile_username);
        txtAbout = (TextView) getView().findViewById(R.id.profile_about);
        txtFollowingCount = (TextView) getView().findViewById(R.id.profile_following_count);
        txtFollowersCount = (TextView) getView().findViewById(R.id.profile_followers_count);
//        txtNationality = (TextView) getView().findViewById(R.id.profile_nationality);


        if(firebaseUser != null) {
            Log.i("DATA USER PROFILE", ""+slang.getUser().getUsername());
            Picasso.with(getContext()).load(firebaseUser.getPhotoUrl()).into(imgProfile);
            txtUsername.setText(slang.getUser().getUsername());
            txtAbout.setText(slang.getUser().getAbout());
//            txtNationality.setText("Natural de "+slang.getUser().getCountry());
            txtFollowingCount.setText(""+slang.getUser().getFollowing());
            txtFollowersCount.setText(""+slang.getUser().getFollowers());
        }
    }

    @Override
    public void onClickLitener(int position, ArrayList<Post> posts) {
        DialogComments dialog = new DialogComments(posts.get(position));
        dialog.show(getActivity().getFragmentManager(), "Comments");
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

}
