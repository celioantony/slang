package models;

import java.util.ArrayList;

/**
 * Created by Celio Antony on 30/11/2016.
 */

public class Languages {

    private ArrayList<String> languages;

    public Languages (){
    }

    public Languages(ArrayList<String> languages){
        this.languages = languages;
    }

    public ArrayList<String> getLanguages() {
        return languages;
    }

    public void setLanguages(ArrayList<String> languages) {
        this.languages = languages;
    }
}
