package models;

import android.net.Uri;
import android.util.Log;

/**
 * Created by Celio Antony on 15/11/2016.
 */

public class Users {

    private String username;
    private String photo;
    private String email;
    private String uuid;
    private String about;
    private String country;
    private String learning;
    private long following;
    private long followers;

    public Users() {}

    public Users(String username, String photo, String uuid, String email, String about, String country, String learning, long following, long followers) {
        this.username = username;
        this.photo = photo;
        this.uuid = uuid;
        this.email = email;
        this.about = about;
        this.country = country;
        this.learning = learning;
        this.following = following;
        this.followers = followers;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public long getFollowing() {
        return following;
    }

    public void setFollowing(long following) {
        this.following = following;
    }

    public long getFollowers() {
        return followers;
    }

    public void setFollowers(long followers) {
        this.followers = followers;
    }

    public String getLearning() {
        return learning;
    }

    public void setLearning(String learning) {
        this.learning = learning;
    }
}
